<?php
$addOnList = require 'wp-addons.php';

$addOnType = $argv[1];
$addOnAction = $argv[2];

$requestedList = [];
if(!empty($addOnList[$addOnType][$addOnAction])){
    $requestedList = $addOnList[$addOnType][$addOnAction];
    foreach($requestedList as $key => $item){
        if(is_array($item)){
            if(!empty($item[1])){
                $requestedList[$key] = "${item[0]} --version=${item[1]}";
            }
            
        }
    }
}

echo implode("\n", $requestedList);