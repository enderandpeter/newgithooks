#!/bin/bash
commandlist=('plugin install' 'plugin uninstall' 'theme install' 'theme delete')
OLD_IFS="$IFS"

# Run each command for every plugin and theme returned
# from get-wp-addons.php
for command in "${commandlist[@]}"; do
    plugins=$(IFS=$' \n\t';php -f get-wp-addons.php $command)    
    IFS=$'\n'
    for plugin in $plugins; do        
        activate=
        # Add the --activate flag to install commands
        if [ "$(echo $command | cut -d ' ' -f 2)" == "install" ]; then
            activate='--activate'
        fi
        echo wp $command $plugin $activate
    done
done
IFS=$OLD_IFS